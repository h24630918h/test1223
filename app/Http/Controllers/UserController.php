<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\UserService;

use Validator;

class UserController extends Controller
{
    public $userService;

    public function __construct()
    {
        $this->userService = new UserService();
    }

    //region 會員註冊

    /**
     * 會員註冊
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function register(Request $request)
    {
        $postData = $request->all();
        $objValidator = Validator::make(
            $postData,
            [
                'account' => [
                    'required',
                    'between:4,12',
                    'regex:/^[\x7f-\xffA-Za-z0-9]+$/',
                    'string',
                    'unique:users'
                ],
                'password' => [
                    'required',
                    'between:8,12',
                    'regex:/^[\x7f-\xffA-Za-z0-9]+$/',
                    'string',
                    'confirmed'
                ],
                'name' => 'required|max:10|string',
                'active' => 'integer|between:1,2',

            ]
        );

        if ($objValidator->fails()) {
            return $this->responseWithJson(array('error' => $objValidator->errors(), 'code' => config('apiCode.validateFail')));
        }

        return $this->responseWithJson($this->userService->register($postData));
    }
    //endregion

    //region 新增帳號

    /**
     * 新增帳號
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function newUser(Request $request)
    {
        $postData = $request->all();
        $objValidator = Validator::make(
            $postData,
            [
                'account' => [
                    'required',
                    'between:4,12',
                    'regex:/^[\x7f-\xffA-Za-z0-9]+$/',
                    'string',
                    'unique:users'
                ],
                'password' => [
                    'required',
                    'between:8,12',
                    'regex:/^[\x7f-\xffA-Za-z0-9]+$/',
                    'string',
                    'confirmed'
                ],
                'name' => 'required|max:10|string',
                'active' => 'integer|between:1,2',
            ]
        );

        if ($objValidator->fails()) {
            return $this->responseWithJson(array('error' => $objValidator->errors(), 'code' => config('apiCode.validateFail')));
        }
        return $this->responseWithJson($this->userService->register($postData));
    }
    //endregion


    // region 登入

    /**
     * 會員登入
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function login(Request $request)
    {
        $postData = $request->only('account', 'password');
        $objValidator = Validator::make(
            $postData,
            [
                'account' => [
                    'required',
                    'between:4,12',
                    'regex:/^[\x7f-\xffA-Za-z0-9]+$/',
                    'string'
                ],
                'password' => [
                    'required',
                    'between:8,12',
                    'regex:/^[\x7f-\xffA-Za-z0-9]+$/',
                    'string',
                ]
            ]
        );

        if ($objValidator->fails()) {
            return $this->responseWithJson(array('error' => $objValidator->errors(), 'code' => config('apiCode.validateFail')));
        }

        return $this->responseWithJson($this->userService->login($postData, $request->ip()));

    }
    //endregion


    //region 取得全部會員
    /**
     * 取得全部會員
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getAllUser()
    {
        return $this->responseWithJson($this->userService->getAllUser());
    }
    //endregion

    //region 取得會員資料 by帳號
    /**
     * 取得會員資料 by帳號
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getUserByAccount(Request $request)
    {

        $postData = array("account" => $request->account);
        $objValidator = Validator::make(
            $postData,
            [
                'account' => [
                    'required',
                    'between:4,12',
                    'regex:/^[\x7f-\xffA-Za-z0-9]+$/',
                    'string'
                ],
            ]
        );

        if ($objValidator->fails()) {
            return $this->responseWithJson(array('error' => $objValidator->errors(), 'code' => config('apiCode.validateFail')));
        }

        return $this->responseWithJson($this->userService->getUserByAccount($postData));
    }
    //endregion


    //region  修改會員資料

    /**
     *  修改會員資料
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editUser(Request $request)
    {
        $postData = $request->all();
        $objValidator = Validator::make(
            $postData,
            [
                'password' => [
                    'between:8,12',
                    'regex:/^[\x7f-\xffA-Za-z0-9]+$/',
                    'string',
                    'confirmed'
                ],
                'name' => 'required|max:10|string',
            ]
        );

        if ($objValidator->fails()) {
            return $this->responseWithJson(array('error' => $objValidator->errors(), 'code' => config('apiCode.validateFail')));
        }
        return $this->responseWithJson($this->userService->editUser($postData));
    }
    //endregion

    //region  刪除會員資料 & 使用者日誌

    /**
     *   刪除會員資料 & 使用者日誌
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function deleteUser(Request $request)
    {
        $postData = $request->all();
        $objValidator = Validator::make(
            $postData,
            [
                'account' => [
                    'required',
                    'between:4,12',
                    'regex:/^[\x7f-\xffA-Za-z0-9]+$/',
                    'string'
                ],
            ]
        );
        if ($objValidator->fails()) {
            return $this->responseWithJson(array('error' => $objValidator->errors(), 'code' => config('apiCode.validateFail')));
        }
        return $this->responseWithJson($this->userService->deleteUser($postData));


    }
    //endregion

    //region 取得使用者日誌

    /**
     *   取得使用者日誌
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getDaily(Request $request)
    {
        $postData = $request->all();
        $objValidator = Validator::make(
            $postData,
            [
                'account' => [
                    'required',
                    'between:4,12',
                    'regex:/^[\x7f-\xffA-Za-z0-9]+$/',
                    'string'
                ],
                'time_start' => 'required|date_format:"Y-m-d H:i:s"',
                'time_end' => 'required|date_format:"Y-m-d H:i:s"',
            ]
        );

        if ($objValidator->fails()) {
            return $this->responseWithJson(array('error' => $objValidator->errors(), 'code' => config('apiCode.validateFail')));
        }
        return $this->responseWithJson($this->userService->getDaily($postData));
    }
    //endregion


}


