<?php

namespace App\Repositories\Platform;

use DB;

use App\Entities\Model\UserDaily;
use App\Repositories\Repository;

class UserDailyRepository
{
    use Repository;

    public function __construct()
    {
        $this->setEntity(UserDaily::class);
    }
}
