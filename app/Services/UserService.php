<?php


namespace App\Services;

use App\Repositories\Platform\UserRepository;
use App\Repositories\Platform\UserDailyRepository;
use Tymon\JWTAuth\Facades\JWTAuth;
use Hash;
use Auth;

class UserService
{

    private $userRepository;
    private $userDailyRepository;

    public function __construct()
    {
        $this->userRepository = new UserRepository();
        $this->userDailyRepository = new UserDailyRepository();
    }

    #region  會員註冊
    /**
     * 會員註冊
     *
     * @param array $postData
     * @return array
     */
    public function register($postData)
    {
        try {

            $postData['password'] = bcrypt($postData['password']);
            if (!isset($postData['active']))
                $postData['active'] = 1;

            $newUser = $this->userRepository->create($postData);
            return array('result' => array("token" => JWTAuth::fromUser($newUser), "user" => $newUser), 'code' => config('apiCode.success'));


        } catch (JWTException $e) {
            return array('error' => $e->getMessage(), 'code' => $e->getCode() ?? config('apiCode.couldNotCreateToken'));
        } catch (Exception $e) {
            return array('error' => $e->getMessage(), 'code' => config('apiCode.notAPICode'));
        } catch (Throwable $t) {
            return array('error' => $t->getMessage(), 'code' => config('apiCode.ServiceUnavailable'));
        }
    }
    #endregion

    #region  會員登入
    /**
     * 會員登入
     *
     * @param array $postData
     * @param string $ip
     * @return array
     */
    public function login($postData, $ip)
    {
        try {

            $user = $this->userRepository->getUserByAccount($postData['account']);

            if ($user) {
                if ($user->active == 2) {
                    return array('error' => "帳號停用中", 'code' => config('apiCode.invalidPermission'));
                }

                if (Hash::check($postData['password'], $user->password)) {
                    //使用者日誌
                    $userDailyData['user_id'] = $user->user_id;
                    $userDailyData['login_ip'] = $ip;

                    $this->userDailyRepository->create($userDailyData);
                    return array('result' => array("token" => JWTAuth::fromUser($user), "user" => $user,), 'code' => config('apiCode.success'));

                }
                return array('error' => "密碼錯誤", 'code' => config('apiCode.invalidCredentials'));
            }
            return array('error' => "查無此會員", 'code' => config('apiCode.memberNotFound'));

        } catch (JWTException $e) {
            return array('error' => $e->getMessage(), 'code' => $e->getCode() ?? config('apiCode.couldNotCreateToken'));
        } catch (Exception $e) {
            return array('error' => $e->getMessage(), 'code' => config('apiCode.notAPICode'));
        } catch (Throwable $t) {
            return array('error' => $t->getMessage(), 'code' => config('apiCode.ServiceUnavailable'));
        }
    }
    #endregion

    #region 取得全部會員
    /**
     * 取得全部會員
     *
     * @return array
     */
    public function getAllUser()
    {
        try {

            $getUserData = $this->userRepository->getAllUser();
            return array("result" => $getUserData, 'code' => config('apiCode.success'));

        } catch (Exception $e) {
            return array('error' => $e->getMessage(), 'code' => config('apiCode.notAPICode'));
        } catch (Throwable $t) {
            return array('error' => $t->getMessage(), 'code' => config('apiCode.ServiceUnavailable'));
        }
    }
    #endregion

    #region  取得會員資料 by帳號
    /**
     * 取得會員資料 by帳號
     *
     * @param array $postData
     * @return array
     */
    public function getUserByAccount($postData)
    {
        try {

            $getUserData = $this->userRepository->getUserByAccount($postData['account']);
            return array("result" => $getUserData, 'code' => config('apiCode.success'));

        } catch (Exception $e) {
            return array('error' => $e->getMessage(), 'code' => config('apiCode.notAPICode'));
        } catch (Throwable $t) {
            return array('error' => $t->getMessage(), 'code' => config('apiCode.ServiceUnavailable'));
        }
    }
    #endregion

    #region 修改會員資料
    /**
     * 修改會員資料
     *
     * @param array $postData
     * @return array
     */
    public function editUser($postData)
    {
        try {
            $myData = $this->userRepository->getUserByAccount($postData['user']['account']);
            if (!$myData) {
                return array("error" => 0, '查無此資料' => config('apiCode.notFound'));
            }

            if (!Hash::check($postData['password'], $myData->password)) {
                $myData->password = bcrypt($postData['password']);
                $myData->active = 2;
            }
            $myData->name = $postData['name'];
            $myData->save();
            return array("result" => 1, 'code' => config('apiCode.success'));


        } catch (Exception $e) {
            return array('error' => $e->getMessage(), 'code' => config('apiCode.notAPICode'));
        } catch (Throwable $t) {
            return array('error' => $t->getMessage(), 'code' => config('apiCode.ServiceUnavailable'));
        }
    }
    #endregion

    #region 刪除會員資料 & 使用者日誌
    /**
     * 刪除會員資料 & 使用者日誌
     *
     * @param array $postData
     * @return array
     */
    public function deleteUser($postData)
    {
        try {
            if (strpos($postData['account'], 'admin') !== false) {
                return array("error" => '無法刪除admin', 'code' => config('apiCode.invalidPermission'));
            }

            $deleteData = $this->userRepository->getUserByAccount($postData['account']);

            if (!$deleteData) {
                return array("error" => '查無此資料', 'code' => config('apiCode.notFound'));
            }

            $deleteData->delete();
            return array("result" => 1, 'code' => config('apiCode.success'));

        } catch (Exception $e) {
            return array('error' => $e->getMessage(), 'code' => config('apiCode.notAPICode'));
        } catch (Throwable $t) {
            return array('error' => $t->getMessage(), 'code' => config('apiCode.ServiceUnavailable'));
        }
    }
    #endregion

    #region 取得使用者日誌
    /**
     * 取得使用者日誌
     *
     * @param array $postData
     * @return array
     */
    public function getDaily($postData)
    {
        try {

            $getDailyData = $this->userRepository->getDaily($postData['account'], $postData['time_start'], $postData['time_end']);
            return array("result" => $getDailyData, 'code' => config('apiCode.success'));

        } catch (Exception $e) {
            return array('error' => $e->getMessage(), 'code' => config('apiCode.notAPICode'));
        } catch (Throwable $t) {
            return array('error' => $t->getMessage(), 'code' => config('apiCode.ServiceUnavailable'));
        }
    }
    #endregion

}

