<?php

namespace App\Repositories\Platform;

use DB;
use App\User;
use App\Repositories\Repository;

class UserRepository
{
    use Repository;

    public function __construct()
    {
        $this->setEntity(User::class);
    }


    /**
     * 取得會員資料
     *
     * @param string $account 帳號
     *
     * @return array
     */
    public function getUserByAccount($account)
    {
        return User::where('account', $account)->first();
    }


    /**
     * 取得全部會員
     *
     * @return array
     */
    public function getAllUser()
    {
        return User::orderByDesc('created_at')->get();
    }


    /**
     * 取得會員日誌
     *
     * @param string account 帳號
     * @param DateTime time_start 開始時間
     * @param DateTime time_end 結束時間
     *
     * @return array
     */
    public function getDaily($account, $time_start, $time_end)
    {
        return User::select('users.user_id', 'users.account as user_account', 'users.name as user_name', 'users.active',
            'user_dailies.daily_id', 'user_dailies.login_ip', 'user_dailies.created_at', 'user_dailies.updated_at')
            ->where('account', $account)
            ->whereBetween('user_dailies.created_at', [$time_start, $time_end])
            ->join('user_dailies', 'users.user_id', '=', 'user_dailies.user_id')
            ->get();
    }

    /**
     * 取得會員日誌
     *
     * @param string account 帳號
     * @param DateTime time_start 開始時間
     * @param DateTime time_end 結束時間
     *
     * @return array
     */
    public function getWithDaily($account, $time_start, $time_end)
    {
        return User::select('users.user_id', 'users.account as user_account', 'users.name as user_name', 'users.active')
            ->where('users.account', $account)
            ->with(['user_dailies' => function ($query) use ($time_start, $time_end) {
                $query->select(['user_id', 'daily_id', 'login_ip', 'created_at', 'updated_at'])
                    ->whereBetween('created_at', [$time_start, $time_end])
                    ->orderBy('created_at')->get();
            }])->get();
    }

}
