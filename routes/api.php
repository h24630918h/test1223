<?php

use Illuminate\Http\Request;


Route::group(['middleware' => ['displayChinese', 'cors']], function () {
    Route::group(['prefix' => 'v1'], function () {
        Route::post('user/register', 'UserController@register');
        Route::post('user/login', 'UserController@login');

        Route::group(['middleware' => 'jwt_auth'], function () {
            Route::post('user', 'UserController@newUser');
            Route::get('user', 'UserController@getAllUser');
            Route::get('user/{account}', 'UserController@getUserByAccount');
            Route::put('user', 'UserController@editUser');
            Route::delete('user', 'UserController@deleteUser');
            Route::post('user/user-login', 'UserController@getDaily');
        });
    });
});


