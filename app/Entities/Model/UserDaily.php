<?php


namespace App\Entities\Model;
use App\Entities\Model\PlatformModel;

class UserDaily extends PlatformModel
{
    protected $table = 'user_dailies';
    protected $primaryKey = 'daily_id';
    protected $fillable = [
        'user_id', 'login_ip'
    ];
    protected $hidden = [
        'delete_at'
    ];
}
